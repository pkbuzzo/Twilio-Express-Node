//Dependencies:
//yarn add express cors twilio

const express = require("express");
const cors = require("cors");
const twilio = require("twilio");

//twilio requirements -- Texting API
const accountSid = "AC9c8ee43ab90db0c7b42ab3bc98e49615";
const authToken = "8579799817f3cdf66360e330b9582722";
const client = new twilio(accountSid, authToken);

const app = express(); //alias

app.use(cors()); //Blocks browser from restricting any data

//Welcome Page for the Server
app.get("/", (req, res) => {
  res.send("CharCode SMS Server Is Running!");
});

//Twilio
app.get("/send-text", (req, res) => {
  //Welcome Message
  res.send("CHARCODE ALERT: ");

  //_GET Variables
  const { recipient, textmessage } = req.query;

  //Send Text
  client.messages
    .create({
      body: textmessage,
      to: recipient, // Text this number
      from: "+19196364260" // From a valid Twilio number
    })
    .then(message => console.log(message.body));
});

app.listen(3000, () => console.log("Running on Port 3000"));
